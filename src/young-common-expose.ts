/*
 * @Author: zhangyang
 * @Date: 2021-08-27 11:33:31
 * @LastEditTime: 2022-03-03 17:03:30
 * @Description: 统一暴露给自动导入插件使用
 */
export { deepClone } from './utils/deepClone';
export { isArray } from './utils/isType';
export { isJsonStr } from './utils/valid';
export { sleep } from './utils/tool';
