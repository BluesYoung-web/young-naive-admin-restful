/*
 * @Author: zhangyang
 * @Date: 2021-12-06 18:50:15
 * @LastEditTime: 2022-05-31 11:36:29
 * @Description: 表单相关操作解耦
 */
import type { MaybeRef } from '@vueuse/shared';
import type { FormInst } from 'naive-ui';
import { setDialogCenter } from '../layouts/components/hooks/useUser';
type Cbk<T extends any> = {
  addCbk?: () => Promise<void | boolean>;
  modCbk?: () => Promise<void | boolean>;
  delCbk?: (row: T) => Promise<void>;
  cpEffect?: (row: T) => void | Promise<void>;
  cgEffect?: () => void | Promise<void>;
  extraClear?: () => void;
  disableclear?: boolean;
};

type ValidFn = () => Promise<boolean>;
export default <T>(
  FORM_TEMP: T,
  { addCbk, modCbk, delCbk, cpEffect, cgEffect, extraClear, disableclear }: Cbk<T>,
  tip: MaybeRef<string> = '确认删除该条数据？'
) => {
  const isAdd = ref(false);
  const isEdit = ref(false);
  const isMore = ref(false);

  const form = ref<T>(deepClone(FORM_TEMP));
  const formRef = ref<FormInst>();
  const validForm: ValidFn = async () => {
    const res = await new Promise((resolve) => {
      formRef.value?.validate(async (err) => {
        if(!err) {
          resolve(true);
        }
      }).catch((err) => {
        resolve(false);
      });
    });
    return res as boolean;
  };

  const clear = () => {
    isAdd.value = false;
    isEdit.value = false;
    isMore.value = false;
    extraClear?.();
    // @ts-ignore
    form.value = deepClone(FORM_TEMP);
  };

  const del = (row: T) => {
    window.$alert.warning({
      title: '提示',
      content: () => unref(tip),
      'positive-text': '确认',
      'negative-text': '取消',
      'onPositiveClick': async () => {
        await delCbk?.(row);
        cgEffect?.();
        return true;
      }
    }).style = setDialogCenter({ top: '20vh' });
  };

  const sure = async () => {
    if (isAdd.value) {
      const res = await addCbk?.();
      if (res === false) {
        return;
      }
    } else {
      const res = await modCbk?.();
      if (res === false) {
        return;
      }
    }
    !disableclear && clear();
    cgEffect?.();
  };

  const edit = async (row: T) => {
    // @ts-ignore
    form.value = deepClone(row);
    await cpEffect?.(row);
    isEdit.value = true;
  };
  const more = async (row: T) => {
    // @ts-ignore
    form.value = deepClone(row);
    await cpEffect?.(row);
    isMore.value = true;
  };
  return {
    isAdd,
    isEdit,
    isMore,
    clear,
    edit,
    more,
    form,
    del,
    sure,
    formRef,
    validForm
  };
};
