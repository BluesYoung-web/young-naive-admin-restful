/*
 * @Author: zhangyang
 * @Date: 2022-05-18 19:15:00
 * @LastEditTime: 2022-06-25 13:49:12
 * @Description: 公共配置
 */
import UiwAppstoreO from '~icons/uiw/appstore-o';
import Dashboard from '~icons/ant-design/dashboard-outlined';
import Comp from '~icons/radix-icons/component-1';
import TreeTable from '~icons/ri/node-tree';
import Peoples from '~icons/ph/users';
import User from '~icons/ph/user-light';
import Tree from '~icons/entypo/flow-tree';
import Example from '~icons/ep/help-filled';
import Config from '~icons/eos-icons/configuration-file-outlined';
import Nav from '~icons/lucide/navigation';
import Imgs from '~icons/bi/images';
import Uni from '~icons/jam/universe';
import type { Component } from 'vue';

export const Icons: Record<string, Component> = {
  'store': UiwAppstoreO,
  'dashboard': Dashboard,
  'component': Comp,
  'tree-table': TreeTable,
  'peoples': Peoples,
  'user': User,
  'tree': Tree,
  'example': Example,
  'config': Config,
  'nav': Nav,
  'imgs': Imgs,
  'uni': Uni
};

export const disableCsrf = import.meta.env.VITE_DISABLE_CSRF === 'true';

export const enableCasdoor = import.meta.env.VITE_ENABLE_CASDOOR === 'true';

export const casdoorConf = {
  serverUrl: import.meta.env.VITE_CAS_SERVER,
  clientId: import.meta.env.VITE_CAS_ID,
  organizationName: import.meta.env.VITE_CAS_ORG,
  appName: import.meta.env.VITE_CAS_APP
};

export const methodObj: Record<string, string> = {
  'GET': 'info',
  'POST': 'success',
  'PATCH': 'warning',
  'DELETE': 'error',
};