
/*
 * @Author: zhangyang
 * @Date: 2022-03-01 16:28:37
 * @LastEditTime: 2022-03-06 14:35:27
 * @Description: 提示指令化
 */
import { useMessage } from 'naive-ui';

export default defineComponent({
  setup() {
    // @ts-ignore
    window.$message = useMessage();
    return () => <div></div>;
  }
});