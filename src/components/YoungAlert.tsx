/*
 * @Author: zhangyang
 * @Date: 2022-03-01 16:39:05
 * @LastEditTime: 2022-03-01 16:39:06
 * @Description: 提示弹窗指令化
 */
import { useDialog } from 'naive-ui';

export default defineComponent({
  setup() {
    // @ts-ignore
    window.$alert = useDialog();
    return () => <div></div>;
  }
});