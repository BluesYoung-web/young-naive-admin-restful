/*
 * @Author: zhangyang
 * @Date: 2022-03-01 16:39:05
 * @LastEditTime: 2022-03-01 16:39:06
 * @Description: 提示弹窗指令化
 */
import { useLoadingBar } from 'naive-ui';

export default defineComponent({
  setup() {
    window.$loadingBar = useLoadingBar();
    return () => <div></div>;
  }
});