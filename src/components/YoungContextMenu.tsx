/*
 * @Author: zhangyang
 * @Date: 2022-03-03 19:52:51
 * @LastEditTime: 2022-03-04 10:13:38
 * @Description: 自定义上下文菜单组件
 */

import type { Cbk } from '@/typings/type';
import type { PropType } from 'vue';
import { Teleport } from 'vue';
export type ContextMenuItem = {
  handler: Cbk;
  title: string;
};

export default defineComponent({
  props: {
    modelValue: { type: Boolean, required: true },
    menuList: { type: Object as PropType<ContextMenuItem[]>, required: true }
  },
  emits: ['update:modelValue'],
  setup(props, { emit }) {
    const { x, y } = useMouse();
    const left = ref(0);
    const top = ref(0);
    const menu = ref();

    const class_cover = ['absolute', 'w-100vw', 'h-100vh', 'top-0', 'z-1001'];
    const class_menu = [
      'm-0', 'bg-white', 'z-3000', 'absolute', 'list-none',
      'px-1', 'rounded', 'text-xs'
    ];

    watch(() => props.modelValue, (newVal, oldVal) => {
      if (newVal && !oldVal) {
        nextTick(() => {
          const { width, height } = window.getComputedStyle(menu.value as HTMLElement);
          const { innerWidth, innerHeight } = window;
          // 此时鼠标的坐标
          const tx = x.value;
          const ty = y.value;
          // 此时自定义菜单的宽高
          const rw = parseFloat(width);
          const rh = parseFloat(height);
          // 处理边界值
          left.value = innerWidth - tx > rw ? tx : innerWidth - rw;
          top.value = innerHeight - ty > rh ? ty : innerHeight - rh;
        });
      }
    });

    const clickHandler = (event: MouseEvent, handler: Cbk) => {
      event.preventDefault();
      handler();
    };
    const close = () => {
      emit('update:modelValue', false);
    };

    return () => (
      <Teleport to="body">
        <div
          style={{
            display: props.modelValue ? 'block' : 'none',
            backgroundColor: 'rgba(200, 200, 200, 0)'
          }}
          class={class_cover}
          onClick={() => close()}
          onContextmenu={(e) => e.preventDefault()}
        >
          <ul
            ref={menu}
            style={{
              left: left.value + 'px',
              top: top.value + 'px',
              color: '#333',
              boxShadow: '2px 2px 3px 0 rgba(0, 0, 0, .3)',
            }}
            class={class_menu}
          >
            {
              props.menuList.map((item, index) =>
                <li
                  key={index + 'dfasrwe'}
                  class="m-0 py-7px px-16px cursor-pointer"
                  onClick={(e) => clickHandler(e, item.handler)}
                >{item.title}</li>
              )
            }
          </ul>
        </div>
      </Teleport>
    )
  }
});