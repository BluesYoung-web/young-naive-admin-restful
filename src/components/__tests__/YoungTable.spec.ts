/*
 * @Author: zhangyang
 * @Date: 2022-03-09 19:21:21
 * @LastEditTime: 2022-03-10 09:15:16
 * @Description: 基础表格组件测试
 */
import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';

import YoungTable from '../YoungTable.vue';
import type { TableDataItem, TableHeadItem } from '@/typings/table';

type HeadItem = {
  name: string;
  age: number;
  address: string;
};
const tableHead: TableHeadItem<HeadItem>[] = [
  { prop: 'name', label: '姓名', width: '32px' },
  { prop: 'age', label: '年龄' },
  { prop: 'address', label: '地址' }
];
const tableData: TableDataItem<HeadItem>[] = [
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
];

describe('基础使用', () => {
  const wrapper = mount(YoungTable, {
    props: {
      tableHead,
      tableData
    }
  });

  it('基本参数', () => {
    const {
      bottomBordered,
      bordered,
      singleColumn,
      singleLine,
      size,
      striped,
      tableHead,
      tableData
    } = wrapper.vm;
    expect(bottomBordered).toEqual(true);
    expect(bordered).toEqual(true);
    expect(singleColumn).toEqual(false);
    expect(singleLine).toEqual(true);
    expect(size).toEqual('medium');
    expect(striped).toEqual(false);
    expect(tableHead.length).toEqual(3);
    expect(tableData.length).toEqual(8);
  });

  it('表头渲染', () => {
    const headElement = wrapper.findAll('th');

    const head = headElement.map((item) => item.element.innerHTML);
    expect(head).toContainEqual('姓名');
    expect(head).toContainEqual('年龄');
    expect(head).toContainEqual('地址');

    const first = headElement[0].element;
    expect(first.style.width).toEqual('32px');
  });
});
