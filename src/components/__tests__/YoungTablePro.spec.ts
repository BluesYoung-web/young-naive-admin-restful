/*
 * @Author: zhangyang
 * @Date: 2022-03-10 15:39:54
 * @LastEditTime: 2022-03-10 16:14:45
 * @Description: 高阶表格组件测试
 */
import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';

import YoungTablePro from '../YoungTablePro.vue';
import type { TableDataItem, TableHeadItemPro } from '@/typings/table';
import type { BaseQuery } from '@/typings/type';

type Head = {
  name: string;
  age: number;
  address: string;
};
const tableHead: TableHeadItemPro<Head>[] = [
  { key: 'name', title: '姓名' },
  { key: 'age', title: '年龄' },
  { key: 'address', title: '地址' }
];
const tableData: TableDataItem<Head>[] = [
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
  { name: 'sds', age: 19, address: 'xxxxxxxxxxxx' },
];

describe('基础使用', () => {
  const query = reactive<BaseQuery>({
    page: 1,
    limit: 10,
    total: 100
  });

  const wrapper = mount(YoungTablePro, {
    props: {
      tableHead,
      tableData,
      ...query,
      'onUpdate:page': (p: number) => query.page = p,
      'onUpdate:limit': (p: number) => query.limit = p,
    }
  });

  it('表头渲染', () => {
    const headElement = wrapper.findAll('th');
    const head = headElement.map((item) => item.element.innerHTML);
    expect(head[0]).toContain('姓名');
    expect(head[1]).toContain('年龄');
    expect(head[2]).toContain('地址');
  });

  it('分页器', async () => {
    expect(wrapper.find('.n-data-table__pagination')).toBeDefined();

    const [pre, next] = wrapper.findAll('.n-pagination-item--button');
    await next.trigger('click');
    await nextTick(() => {
      const activeEl = wrapper.find('.n-pagination-item--active');
      expect(activeEl.element.innerHTML).toEqual('2');
      expect(query.page).toEqual(2);
    });

    await pre.trigger('click');
    await nextTick(() => {
      const activeEl = wrapper.find('.n-pagination-item--active');
      expect(activeEl.element.innerHTML).toEqual('1');
      expect(query.page).toEqual(1);
    });
  });
});
