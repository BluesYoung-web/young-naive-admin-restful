/*
 * @Author: zhangyang
 * @Date: 2022-03-07 15:20:00
 * @LastEditTime: 2022-03-07 16:07:41
 * @Description: 基础测试
 */
import { describe, it, expect } from 'vitest';

describe('Base', () => {
  it('1 + 1', () => {
    expect(1 + 1).toBe(2);
  });
});
