
/*
 * @Author: zhangyang
 * @Date: 2022-03-01 16:28:37
 * @LastEditTime: 2022-03-01 16:35:43
 * @Description: 通知指令化
 */
import { useNotification } from 'naive-ui';

export default defineComponent({
  setup() {
    // @ts-ignore
    window.$notice = useNotification();
    return () => <div></div>;
  }
});