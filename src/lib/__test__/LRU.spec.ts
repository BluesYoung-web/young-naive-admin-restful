/*
 * @Author: zhangyang
 * @Date: 2022-03-22 17:05:12
 * @LastEditTime: 2022-03-23 09:45:47
 * @Description: 手写 LRU
 */
import { it, describe, expect } from 'vitest';
import { LRU } from '../LRU';

describe('LRU', () => {
  it('初始化', () => {
    const lru = new LRU();
    expect(lru).toBeDefined();
    expect(lru.maxLength).toBe(3);
    expect(lru.get).toBeInstanceOf(Function);
    expect(lru.put).toBeInstanceOf(Function);
    expect(lru.all).toBeInstanceOf(Function);
    expect(lru.clear).toBeInstanceOf(Function);
  });

  it('基础使用：不存在新增，存在覆盖', () => {
    const lru = new LRU();
    expect(lru.get(1)).toBeUndefined();
    lru.put(1, 1);
    expect(lru.get(1)).toBe(1);
    lru.put(1, 3);
    expect(lru.get(1)).toBe(3);
  });

  it('依次存入，直至溢出', () => {
    const lru = new LRU(5);
    expect(lru.maxLength).toBe(5);
    lru.put(1, 1);
    lru.put(2, 2);
    lru.put(3, 3);
    lru.put(4, 4);
    lru.put(5, 5);
    expect(lru.all().size).toBe(5);
    expect([...lru.all().entries()]).toEqual([
      [1, 1],
      [2, 2],
      [3, 3],
      [4, 4],
      [5, 5]
    ]);

    lru.put(6, 6);
    expect([...lru.all().entries()]).toEqual([
      [2, 2],
      [3, 3],
      [4, 4],
      [5, 5],
      [6, 6]
    ]);
  });

  it('删除', () => {
    const lru = new LRU();
    lru.put(1, 1);
    expect(lru.get(1)).toBe(1);
    expect(lru.delete(1)).toBe(true);
    expect(lru.delete(1)).toBe(false);
    expect(lru.get(1)).toBeUndefined();

    lru.put(1, 1);
    lru.put(2, 2);
    lru.put(3, 3);
    lru.put(4, 4);
    lru.put(5, 5);
    expect(lru.all().size).toBe(3);
    expect([...lru.all().entries()]).toEqual([
      [3, 3],
      [4, 4],
      [5, 5]
    ]);

    lru.clear();
    expect(lru.all().size).toBe(0);
  });

  it('加权删除', () => {
    const lru = new LRU();
    lru.put(1, 1);
    lru.put(2, 2);
    lru.put(3, 3);

    lru.get(1);
    lru.put(4, 4);
    expect([...lru.all().entries()]).toEqual([
      [1, 1],
      [3, 3],
      [4, 4]
    ]);

    lru.get(3);
    lru.put(5, 5);
    expect([...lru.all().entries()]).toEqual([
      [1, 1],
      [3, 3],
      [5, 5]
    ]);
  });
});