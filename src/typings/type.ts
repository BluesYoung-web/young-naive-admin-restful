/*
 * @Author: zhangyang
 * @Date: 2022-03-01 14:03:04
 * @LastEditTime: 2022-05-31 15:59:31
 * @Description: 部分类型定义
 */
import type { App } from 'vue';
import type { Icons } from '@/conf';
import type { Method } from 'axios';

export type UserModule = (ctx: App, ...args: any[]) => void;
export type LoginType = 'code' | 'ding';
export type Cbk = () => void;

export type ResponseObj = {
  code: number;
  data: any;
  msg: string;
};

export type UserKey = {
  expires: string;
  token: string;
};

export type CurrUserInfo = {
  avatar: string;
  id: number;
  introduction: string;
  mobile: string;
  nickname: string;
  roles: string[];
  roleSort: number;
  username: string;
};

export type UserItem = {
  id: number;
  username: string;
  nickname: string;
  mobile: string;
  roleId: number;
  status: number;
  role_name?: string;
  creator?: string;
  newPassword?: string;
  initPassword?: string;
};

export type NavArrItem = {
  breadcrumb: number;
  component: string;
  createdAt: string;
  creator: string;
  icon?: keyof typeof Icons | any;
  id: number;
  name: string;
  not_dev: number;
  parentId: number;
  path: string;
  permission: string;
  redirect: string;
  sort: number;
  status: number;
  title?: string;
  updatedAt: string;
  visible: number;
  children?: NavArrItem[] | [];
} & Obj;

export type RoleItem = {
  createdAt?: string;
  creator?: string;
  desc: string;
  id: number;
  keyword: string;
  name: string;
  not_dev?: number;
  sort?: number;
  status: number;
  updatedAt?: string;
};

export type ApiItem = {
  id: number;
  method: Method;
  path: string;
  desc: string;
  category: string;
  roleIds: number[];
  creator?: string;
  title?: string;
};

export type EnableWrite<T extends any> = {
  -readonly [p in keyof T]: T[p];
};

export interface BaseQuery {
  pageNum: number;
  pageSize: number;
  total: number;
};

export type PagesData = {
  list: any
} & BaseQuery;

export type Obj = Record<string, any>;