/*
 * @Author: zhangyang
 * @Date: 2022-03-09 17:55:56
 * @LastEditTime: 2022-05-31 09:28:02
 * @Description: 自定义表格组件相关
 */

import type { TableColumn } from "naive-ui/lib/data-table/src/interface";
import type { RenderFunction } from 'vue';
import type { Obj } from "./type";

export type TableSize = 'small' | 'medium' |'large';

export type TableHeadItem<T extends any = any> = {
  prop: keyof T;
  label: string;
  width?: string;
};

export type TableDataItem<T extends any = any> = {
  [key in keyof T]: T[key];
} & Obj;

// type DisabledFn<T> = (row: TableDataItem<T>, index: number) => boolean;
// type CompareFn<T> = ((a: TableDataItem<T>, b: TableDataItem<T>) => number);
// type FilterItem<T> = { label: string; value: T };
// type FilterFn<T> = (value: string, row: TableDataItem<T>) => boolean;
// export type TableHeadItemPro<T extends any = any> = {
//   type?: 'selection' | 'expand';
//   disabled?: DisabledFn<T>;
//   fixed?: 'left' | 'right' | false;
//   sortOrder?: 'descend' | 'ascend' | false;
//   sorter?: boolean | CompareFn<T> | {
//     compare: CompareFn<T>,
//     multiple: number;
//   };
//   filterOptions?: FilterItem<keyof T>;
//   filter?: FilterFn<T>;
// } & TableHeadItem<T>;

export type TableHeadItemPro<T extends any = any> = {
  key: keyof T;
  title: string | RenderFunction;
} & TableColumn<T> | TableColumn<T>;