/*
 * @Author: zhangyang
 * @Date: 2022-02-28 17:19:35
 * @LastEditTime: 2022-08-04 08:32:27
 * @Description: 应用入口文件
 */
// 根组件
import App from './App.vue';
// 样式
import '@unocss/reset/tailwind.css';
import 'uno.css';
import type { UserModule } from '@/typings';

const app = createApp(App);
Object.values(
  // 模块按数字命名，确保安装的顺序
  import.meta.glob<{ install: UserModule }>('./modules/*.ts', { eager: true })
).map(({ install }) => install(app));

// 防止组件库样式被覆盖
const meta = document.createElement('meta');
meta.name = 'naive-ui-style';
document.head.appendChild(meta);

app.mount('#app');
