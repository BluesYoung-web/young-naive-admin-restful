/*
 * @Author: zhangyang
 * @Date: 2022-03-01 14:01:31
 * @LastEditTime: 2022-03-01 19:36:47
 * @Description: 状态管理
 */
import type { UserModule } from '@/typings/type';
export const install: UserModule = (app) => {
  const pinia = createPinia();
  app.use(pinia);
};