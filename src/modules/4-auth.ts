/*
 * @Author: zhangyang
 * @Date: 2022-03-01 19:40:13
 * @LastEditTime: 2022-06-23 14:40:51
 * @Description: 权限校验
 */
import { router } from './1-router';
import type { NavArrItem, Obj, UserKey, UserModule } from '@/typings';
import type { RouteLocationNormalized } from 'vue-router';
import { getToken, TopIndex, useNavStore, useTagsStore, useUserStore } from '@/stores';
import { casdoorLogin } from '@/api/base';
import { YoungAuth } from '@bluesyoung/casdoor-auth';
import { casdoorConf, enableCasdoor } from '@/conf';
import { getCurrUserInfo } from '@/api/user';
import { getMenuTree } from '@/api/menu';

const changeTitle = (route: RouteLocationNormalized) => {
  document.title = route.meta.title || import.meta.env.VITE_TITLE || '小黑后台';
};

let role_route: string[] = [];
const generateRoleRoute = (arr: NavArrItem[], num?: number): string[] => {
  if (num === 1) {
    role_route = [];
  }
  for (const item of arr) {
    item.component && role_route.push(item.component);
    // 子节点递归遍历
    if (Array.isArray(item.children) && item.children.length > 0) {
      const part = JSON.parse(JSON.stringify(item.children));
      // 尾递归优化
      generateRoleRoute(part);
    }
  }
  return role_route;
};

// 清除没有子元素的children
export const clearChildren = <T extends Obj>(arr: T[]) => {
  for (const item of arr) {
    if (item?.children.length === 0) {
      delete item.children;
    } else if (item.children) {
      clearChildren(item.children)
    }
  }
  return arr;
}

export const generateNavData = async () => {
  const info = await getCurrUserInfo();
  const menu = await getMenuTree();
  // 获取用户信息
  const { CurrUserInfo } = storeToRefs(useUserStore());
  CurrUserInfo.value = info;
  
  const { NavArr, RoleRoute } = storeToRefs(useNavStore());
  
  // 后端获取用户可见节点
  const navArr: NavArrItem[] = menu;
  const routes: string[] = generateRoleRoute(menu, 1);
  
  // 导航数组
  NavArr.value = navArr.slice();
  // 生成角色有权访问的路由
  RoleRoute.value = routes.slice();
};

export const hasPermission = (path: string) => {
  const { RoleRoute } = storeToRefs(useNavStore());

  const roleRoute = RoleRoute.value;
  return roleRoute.includes(path);
};

export const install: UserModule = (app) => {
  router.beforeEach(async (to, from) => {
    // 开始进度条
    window.$loadingBar.start();
    const { code, state } = to.query as Record<string, string>;
    
    if (code && state) {
      await casdoorLogin(code, state) as UserKey;
      getToken() && generateNavData();
      location.search = '';
      changeTitle(to);
      return true;
    }

    if (getToken() || !to.meta.authPath) {
      getToken() && generateNavData();
      // 已登录或者页面无需权限
      changeTitle(to);
      return true;
    } else {
      await new Promise((resolve) => {
        window.$alert.error({
          title: '提示',
          content: '登录信息过期，请重新登录！',
          'positive-text': '确认',
          closable: false,
          'mask-closable': false,
          'onPositiveClick': () => resolve(true)
        });
      });

      if (enableCasdoor) {
        const auth = new YoungAuth(casdoorConf);
        auth.init();
        return false;
      } else {
        location.href = '/base/login';
        return false;
      }
    }
  });

  router.afterEach((to) => {
    const { addView } = useTagsStore();
    addView(to);
    window.$loadingBar.finish();
  });
};
