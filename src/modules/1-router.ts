/*
 * @Author: zhangyang
 * @Date: 2022-03-01 14:01:31
 * @LastEditTime: 2022-05-30 15:03:53
 * @Description: 路由模块
 */
import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router';
import { setupLayouts } from 'virtual:generated-layouts';
import routes from '~pages';

import type { UserModule } from '@/typings/type';

/**
 * 路由元数据类型扩展
 */
declare module 'vue-router' {
  interface RouteMeta {
    /**
     * 页面标题
     */
    title: string;
    /**
     * 是否固定
     */
    affix?: boolean;
    /**
     * 是否禁用缓存
     */
    noCache?: boolean;
    /**
     * 鉴权路径，不设置则为白名单页面
     */
    authPath?: string;
    /**
     * 页面布局，对应 layouts 目录下的布局页面，默认为 default
     */
    layout?: 'default' | 'single'
  }
};

export const finalRoutes = setupLayouts(routes);
export const navMap = new Map<string, string>();
/**
 * 生成权限节点映射表
 */
const generateNavMap = (raw: RouteRecordRaw[], base = '') => {
  for (const route of raw) {
    if (route.children) {
      generateNavMap(route.children, route.path);
    } else {
      const meta = route.meta;
      if (meta && meta.authPath) {
        navMap.set(
          meta.authPath,
          `${base}${route.path.startsWith('/') ? '' : '/'}${route.path}`
        );
      }
    }
  }
};
generateNavMap(finalRoutes);

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: finalRoutes
});

export const install: UserModule = (app) => {
  
  app.use(router);
};