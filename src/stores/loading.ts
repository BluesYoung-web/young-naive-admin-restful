/*
 * @Author: zhangyang
 * @Date: 2022-03-01 14:32:42
 * @LastEditTime: 2022-03-03 19:29:23
 * @Description: 加载状态
 */
export const useLoadingStore = defineStore('useLoadingStore', () => {
  const loading = ref(false);
  const httpPending = ref(false);
  return {
    loading,
    httpPending
  };
});

import.meta.hot && import.meta.hot.accept(acceptHMRUpdate(useLoadingStore, import.meta.hot));