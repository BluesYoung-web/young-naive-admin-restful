/*
 * @Author: zhangyang
 * @Date: 2022-03-09 15:25:46
 * @LastEditTime: 2022-05-30 16:27:43
 * @Description: 系统消息
 */
import type { MsgItem } from '@/layouts/components/Msg';
import { Icons } from '@/conf';
import { defineStore, acceptHMRUpdate } from 'pinia';

export const useMsgStore = defineStore('useMsgStore', () => {
  const msgList = reactive<MsgItem[]>([
    { icon: () => h(Icons.component), msg: '我是通知内容我是通知内容我是通知内容', time: '2022-03-09', cbk: () => window.$message.info('点击了消息') },
    { icon: () => h(Icons.dashboard), msg: '我是通知内容', time: '2022-03-09' },
    { icon: () => h(Icons.example), msg: '我是通知内容', time: '2022-03-09' },
    { icon: () => h(Icons.peoples), msg: '我是通知内容', time: '2022-03-09' },
    { icon: () => h(Icons.tree), msg: '我是通知内容', time: '2022-03-09' },
    { icon: () => h(Icons.store), msg: '我是通知内容', time: '2022-03-09' },
    { icon: () => h(Icons.user), msg: '我是通知内容', time: '2022-03-09' }
  ]);
  return {
    msgList
  };
});

import.meta.hot && import.meta.hot.accept(acceptHMRUpdate(useMsgStore, import.meta.hot));