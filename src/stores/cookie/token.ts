/*
 * @Author: zhangyang
 * @Date: 2020-12-08 11:15:26
 * @LastEditTime: 2022-05-30 14:15:07
 * @Description: 用户身份认证 Cookie
 */
import type { UserKey } from '@/typings';
import Cookies from 'js-cookie';

const TONKEN_KEY = 'www.bluesyoung-web.top';

/**
 * 获取 Token
 */
export const getToken = () => {
  return Cookies.get(TONKEN_KEY);
};

/**
 * 设置 Token
 * @param key 后端返回的用户信息
 */
export const setToken = (key: UserKey) => {
  return Cookies.set(TONKEN_KEY, key.token, {
    // 自动过期
    expires: new Date(key.expires)
  });
};

/**
 * 删除 Token，退出登录
 */
export const removeToken = () => {
  return Cookies.remove(TONKEN_KEY);
};
