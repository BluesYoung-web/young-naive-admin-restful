/*
 * @Author: zhangyang
 * @Date: 2022-03-02 14:49:34
 * @LastEditTime: 2022-05-30 16:37:07
 * @Description: 导航相关
 */
import type { NavArrItem } from '@/typings/type';
import { defineStore, acceptHMRUpdate } from 'pinia';

export const useNavStore = defineStore('useNavStore', () => {
  const NavArr = ref<NavArrItem[]>([]);
  const LeftNav = ref<NavArrItem[]>([]);
  const RoleRoute = ref<string[]>([]);
  return {
    NavArr,
    LeftNav,
    RoleRoute,
  };
});

import.meta.hot && import.meta.hot.accept(acceptHMRUpdate(useNavStore, import.meta.hot));
