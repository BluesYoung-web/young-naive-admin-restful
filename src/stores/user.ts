/*
 * @Author: zhangyang
 * @Date: 2022-02-24 17:49:16
 * @LastEditTime: 2022-03-02 14:56:04
 * @Description: 存储用户相关信息
 */
import { defineStore, acceptHMRUpdate } from 'pinia';
import type { CurrUserInfo } from '@/typings/type';

export const useUserStore = defineStore('useUserStore', () => {
  const CurrUserInfo = ref<CurrUserInfo>();
  return {
    CurrUserInfo
  };
});

import.meta.hot && import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
