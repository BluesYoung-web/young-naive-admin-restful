/*
 * @Author: zhangyang
 * @Date: 2022-03-02 18:42:56
 * @LastEditTime: 2022-06-23 12:27:18
 * @Description: 存储界面相关的数据
 */
const SESSION_PREFIX = '__young_naive_admin_';

export const SESSION_KEYS = {
  collapsed: `${SESSION_PREFIX}collapsed`,
  topIndex: `${SESSION_PREFIX}topIndex`,
  expandNodeKeys: `${SESSION_PREFIX}expandNodeKeys`,
  csrfToken: `${SESSION_PREFIX}csrfToken`
};

export const isCollapse = useSessionStorage(SESSION_KEYS.collapsed, false);

export const TopIndex = useSessionStorage(SESSION_KEYS.topIndex, 1);

export const expandNodeKeys = useSessionStorage<number[]>(SESSION_KEYS.expandNodeKeys, []);

export const CSRF_TOKEN = useSessionStorage(SESSION_KEYS.csrfToken, '');