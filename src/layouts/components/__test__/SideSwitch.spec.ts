/*
 * @Author: zhangyang
 * @Date: 2022-03-22 16:38:10
 * @LastEditTime: 2022-03-22 16:59:09
 * @Description: 
 */
import { it, describe, expect } from 'vitest';
import { shallowMount } from '@vue/test-utils';
import SideSwitch from '../SideSwitch';


describe('侧边栏开关', () => {
  it('基础逻辑', async () => {
    const opened = ref(true);
    const wrapper = shallowMount(SideSwitch, {
      props: {
        modelValue: opened.value,
        'onUpdate:modelValue': (e: boolean) => {
          opened.value = e;
        }
      }
    });
    
    const el = wrapper.find('uiw-menu-unfold-stub');
    
    await el.trigger('click');
    expect(opened.value).toEqual(false);
  });
})