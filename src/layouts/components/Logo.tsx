/*
 * @Author: zhangyang
 * @Date: 2022-03-03 10:37:57
 * @LastEditTime: 2022-05-21 18:43:20
 * @Description: logo 组件
 */
import { isCollapse } from '@/stores';

export default defineComponent({
  setup() {
    const title = import.meta.env.VITE_TITLE ?? '小黑后台';
    const router = useRouter();
    return () => (
      <div class="cursor-pointer" onClick={() => router.push('/')}>
        {
          isCollapse.value
            ? <div class="flex justify-end items-center py-2 px-2/7">
                <div class="i-dashicons-admin-home  text-xl" />
              </div>
            : <div class="flex justify-center items-center py-2">
                <div class="i-dashicons-admin-home text-xl" />
                <h1 class="text-center text-xl">{ title }</h1>
              </div>
        }
      </div>
    );
  }
});