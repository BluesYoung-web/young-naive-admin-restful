/*
 * @Author: zhangyang
 * @Date: 2022-03-09 12:05:03
 * @LastEditTime: 2022-05-21 18:48:41
 * @Description: 系统通知消息列表
 */
import type { Cbk } from '@/typings/type';
import { withModifiers, type PropType, type RenderFunction } from 'vue';
import { NEmpty } from 'naive-ui';
export type MsgItem = {
  icon: RenderFunction;
  msg: string;
  time: string;
  cbk?: Cbk;
};
export default defineComponent({
  props: {
    list: { type: Object as PropType<MsgItem[]>, default: () => [] }
  },
  emits: ['del'],
  setup(props, { emit }) {
    return () => (
    <div class="w-52 h-80 overflow-auto bg-light-200">
      {
        props.list.length > 0
          ? props.list.map((item, index) => (
            <div key={index + 'fewecwe'} class="flex items-center not-last:border-b m-2 hover:cursor-pointer" onClick={() => item.cbk?.()}>
              <div class="mr-4 text-lg">{ item.icon() }</div>
              <div>
                <div class="truncate w-30 font-720">{ item.msg }</div>
                <div class="text-sm text-gray-500">{ item.time }</div>
              </div>
              <div class="i-lucide-delete text-red-400 text-xs hover:cursor-pointer" onClick={withModifiers(() => emit('del', index), ['stop'])} />
            </div>
            ))
          : <div class="flex justify-center items-center h-full">
              <NEmpty description="暂无消息" />
            </div>
      }
    </div>
    );
  }
});