/*
 * @Author: zhangyang
 * @Date: 2022-03-07 16:39:03
 * @LastEditTime: 2022-05-30 15:20:44
 * @Description: 右上角用户相关
 */
import { useUserStore } from '@/stores';
import { NAvatar, NDropdown, NPopover } from 'naive-ui';
import { useMenu, useMsg } from './hooks/useUser';
export default defineComponent({
  setup() {
    const { isFullscreen, isSupported, enter, exit } = useFullscreen();

    const { CurrUserInfo } = storeToRefs(useUserStore());

    const { options, optionHandler } = useMenu();

    const { hasUnreadMessage, messages, readMessage } = useMsg();
    return () => (
    <div class="px-2 text-xl flex justify-center items-center">
      <NPopover placement="bottom-end" trigger="click" raw>
        {{
          'trigger': () => <div
            class={{
              ['text-red-600 transform-gpu animate-bounce']: hasUnreadMessage.value,
              'i-ic-baseline-notifications-none mx-2 hover:cursor-pointer focus:border-none focus:outline-none': true
            }}
            onClick={readMessage}
          />,
          'default': messages
        }}
      </NPopover>
      {
        isSupported
          && isFullscreen.value
            ? <div class="mx-2 i-ic-outline-fullscreen-exit" cursor="pointer" onClick={() => exit()} />
            : <div class="mx-2 i-ic-outline-fullscreen" cursor="pointer" onClick={() => enter()} />
      }
      <NDropdown options={options} onSelect={optionHandler}>
        <div class="inline-flex justify-center items-center mx-2 hover:cursor-pointer">
          <NAvatar color="transparent">
            <div class="text-xl i-noto-v1-man-technologist-light-skin-tone" />
          </NAvatar>
          <div class="text-sm">{ CurrUserInfo.value?.nickname }</div>
          <div class="mr-2 text-sm i-ion-md-arrow-dropdown" />
        </div>
      </NDropdown>
    </div>
    );
  }
});