/*
 * @Author: zhangyang
 * @Date: 2022-03-02 19:47:47
 * @LastEditTime: 2022-05-21 18:49:31
 * @Description: 侧边栏展开-关闭
 */
export default defineComponent({
  props: {
    modelValue: { type: Boolean, default: false }
  },
  emits: ['update:modelValue'],
  setup(props, { emit }) {
    return () => (
      <div
        class="i-uiw-menu-unfold w-10 px-2 font-bold transform transition-transform duration-800 ease-in-out cursor-pointer"
        style={{
          transform: props.modelValue ? 'none' : 'rotate(180deg)'
        }}
        onClick={() => emit('update:modelValue', !props.modelValue)}
      />
    );
  }
})