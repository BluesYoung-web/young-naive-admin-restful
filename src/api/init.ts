/*
 * @Author: zhangyang
 * @Date: 2022-06-26 14:53:52
 * @LastEditTime: 2022-06-26 14:58:46
 * @Description: 
 */
import { requestWithoutToken } from '@/modules/3-net';

const prefix = `/init`;

export const needInit = async () => requestWithoutToken({
  prefix,
  path: ''
}, {}, 'get') as Promise<boolean>;

export const init = async () => requestWithoutToken({
  prefix,
  path: ''
});