/*
 * @Author: zhangyang
 * @Date: 2022-05-17 17:49:36
 * @LastEditTime: 2022-06-01 09:22:26
 * @Description: 
 */
import { basicRequest } from '@/modules/3-net';
import type { CurrUserInfo, Obj, PagesData, UserItem } from '@/typings';
import { useUserStore } from '@/stores';

const prefix = '/user';

export const getCurrUserInfo = async () => {
  const { CurrUserInfo } = storeToRefs(useUserStore());
  if (CurrUserInfo.value?.id) {
    return Promise.resolve(CurrUserInfo.value);
  } else {
    const info = await basicRequest({
      prefix,
      path: '/info'
    }) as CurrUserInfo;
    CurrUserInfo.value = info;
    return info;
  }
};

export const modifyPassword = async (oldPassword: string, newPassword: string) => {
  return basicRequest({
    prefix,
    path: '/changePwd'
  }, {
    newPassword,
    oldPassword
  }, 'put');
};

export const getUserList = async (args: Obj) => {
  return basicRequest({
    prefix,
    path: '/list'
  }, args, 'get') as Promise<PagesData>;
};

export const changeUserItem = async (args: { id: number } & Partial<UserItem>) => {
  return basicRequest({
    prefix,
    path: `/update/${args.id}`
  }, args, 'patch');
};

export const addUserItem = async (args: UserItem) => {
  return basicRequest({
    prefix,
    path: '/create'
  }, args);
};

export const deleteUser = async (ids: string) => {
  return basicRequest({
    prefix,
    path: '/delete/batch'
  }, { ids }, 'delete');
};