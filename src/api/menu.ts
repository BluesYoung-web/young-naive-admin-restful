/*
 * @Author: zhangyang
 * @Date: 2022-05-30 15:12:59
 * @LastEditTime: 2022-05-31 10:05:54
 * @Description: 
 */
import { basicRequest } from '@/modules/3-net';
import type { NavArrItem } from '@/typings';

const prefix = '/menu';

export const getMenuTree = async () => {
  return basicRequest({
    prefix,
    path: '/tree'
  }, {}, 'get');
};

export const getMenuList = async () => {
  return basicRequest({
    prefix,
    path: '/list'
  }, {}, 'get');
};

export const changeMenuItem = async (args: { id: number } & Partial<NavArrItem>) => {
  return basicRequest({
    prefix,
    path: `/update/${args.id}`
  }, args, 'patch');
};

export const addMenuItem = async (args: NavArrItem) => {
  return basicRequest({
    prefix,
    path: '/create'
  }, args);
};

export const deleteMenu = async (ids: string) => {
  return basicRequest({
    prefix,
    path: '/delete/batch'
  }, { ids }, 'delete');
};

export const getRoleMenuTree = async (id: number) => {
  return basicRequest({
    prefix,
    path: `/all/${id}`
  }, {}, 'get');
};