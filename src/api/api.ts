/*
 * @Author: zhangyang
 * @Date: 2022-05-31 14:11:11
 * @LastEditTime: 2022-05-31 16:02:31
 * @Description: 
 */
import { basicRequest } from '@/modules/3-net';
import type { ApiItem, Obj, PagesData } from '@/typings';

const prefix = '/api';

export const getApiList = async (args: Obj) => {
  return basicRequest({
    prefix,
    path: '/list'
  }, args, 'get') as Promise<PagesData>;
};

export const addApiItem = async (args: ApiItem) => {
  return basicRequest({
    prefix,
    path: '/create'
  }, args);
};

export const changeApiItem = async (args: { id: number } & Partial<ApiItem>) => {
  return basicRequest({
    prefix,
    path: `/update/${args.id}`
  }, args, 'patch');
};

export const deleteApi = async (ids: string) => {
  return basicRequest({
    prefix,
    path: '/delete/batch'
  }, { ids }, 'delete');
};

export const getRoleApis = async (id: number) => {
  return basicRequest({
    prefix,
    path: `/all/category/${id}`
  }, {}, 'get');
};