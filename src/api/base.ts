/*
 * @Author: zhangyang
 * @Date: 2022-05-17 15:38:18
 * @LastEditTime: 2022-06-23 15:03:04
 * @Description: 
 */
import { requestWithoutToken } from '@/modules/3-net';
import { removeToken, setToken } from '@/stores';
const prefix = `/base`;

export const login = async (username: string, password: string) => {
  const res = await requestWithoutToken(
    {
      prefix,
      path: '/login'
    },
    {
      username,
      password
    }
  );
  setToken(res);
  return res;
};

export const casdoorLogin = async (code: string, state: string) => {
  const res = await requestWithoutToken(
    {
      prefix,
      path: '/loginCasdoor'
    },
    {
      code,
      state
    }
  );
  setToken(res);
  return res;
};

export const loginOut = async () => {
  // await basicRequest({
  //   prefix,
  //   path: '/logout'
  // });
  removeToken();
  location.href = '/base/login';
};

export const getCsrf = async () => {
  return requestWithoutToken({
    prefix,
    path: '/safeCsrf'
  }, {}, 'GET');
};