/*
 * @Author: zhangyang
 * @Date: 2022-05-31 09:19:49
 * @LastEditTime: 2022-05-31 15:40:40
 * @Description: 
 */
import { basicRequest } from '@/modules/3-net';
import type { Obj, PagesData, RoleItem } from '@/typings';

const prefix = '/role';

export const getRoleList = async (args: Obj) => {
  return basicRequest({
    prefix,
    path: '/list'
  }, args, 'get') as Promise<PagesData>;
};

export const changeRoleItem = async (args: { id: number } & Partial<RoleItem>) => {
  return basicRequest({
    prefix,
    path: `/update/${args.id}`
  }, args, 'patch');
};

export const addRoleItem = async (args: RoleItem) => {
  return basicRequest({
    prefix,
    path: '/create'
  }, args);
};

export const deleteRole = async (ids: string) => {
  return basicRequest({
    prefix,
    path: '/delete/batch'
  }, { ids }, 'delete');
};

export const changeRoleMenu = async (id: number, add: number[], del: number[]) => {
  return basicRequest({
    prefix,
    path: `/menus/update/${id}`
  }, {
    create: add,
    delete: del
  }, 'patch');
};

export const changeRoleApi = async (id: number, add: number[], del: number[]) => {
  return basicRequest({
    prefix,
    path: `/apis/update/${id}`
  }, {
    create: add,
    delete: del
  }, 'patch');
};