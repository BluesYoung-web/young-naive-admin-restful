/*
 * @Author: zhangyang
 * @Date: 2022-02-28 17:19:35
 * @LastEditTime: 2022-06-23 14:50:33
 * @Description: 类型定义
 */
/// <reference types="vite/client" />
/// <reference types="vite-plugin-pages/client" />
/// <reference types="vite-plugin-vue-layouts/client" />
/// <reference types="@vueuse/shared" />
/// <reference types="unplugin-icons/types/vue3" />

/**
 * 自定义环境变量
 */
interface ImportMetaEnv {
  /**
   * HTTP 请求地址
   */
  VITE_BASE_HTTP: string;
  /**
  * 标题
  */
  VITE_TITLE?: string;
  /**
  * websocket 地址
  */
  VITE_BASE_WS?: string;
  /**
   * 开发时真实的请求地址
   */
  VITE_DEV_ADDRESS?: string;
  /**
   * 是否禁用 csrf
   */
  VITE_DISABLE_CSRF?: string;
  
  /**
   * 是否启用 Casdoor
   */
  VITE_ENABLE_CASDOOR?: string;
  /**
   * Casdoor 相关
   */
  VITE_CAS_SERVER?: string;
  VITE_CAS_ID?: string;
  VITE_CAS_ORG?: string;
  VITE_CAS_APP?: string;
}
/**
 * 扩展 window
 */
type VNode = any;
type MessageOptions = {
   /**
    * 是否显示关闭图标
    */
  closable?: boolean;
  /**
    * 一段时间后自动关闭
    * @default 3000 ms
    */
  duration?: number;
  /**
   * 是否展示图标
   */
  showIcon?: boolean;
  /**
   * 图标内容
   */
  icon?: () => VNode;
  /**
   * hover 时不销毁
   */
  keepAliveOnHover?: boolean;
  /**
  * 消息的渲染函数
  */
  render?: () => VNode;
  /**
   * 消息开始消失的回调
   */
  onLeave?: () =>void;
  /**
   * 消失动画结束的回调
   */
  onAfterLeave?: () =>void;
  /**
   * 点击关闭图标的回调
   */
  onClose?: () =>void;
}
type CMessageOptions = {
  type: 'info' | 'success' | 'warning' | 'error' | 'loading' | 'default';
} & MessageOptions;


type NoticeOptions = {
  /**
   * 通知内容
   */
  content: string | (() => VNode);
  /**
   * 标题
   */
  title?: string | (() => VNode);
  /**
   * 描述
   */
  description?: string | (() => VNode);
  /**
   * meta 信息
   */
  meta?: string | (() => VNode);
  /**
   * 一段时间后自动关闭，默认不关闭
   */
  duration?: number;
  /**
   * 是否显示关闭图标
   * @default true
   */
  closable?: boolean;
  /**
   * 操作区域的内容，可为渲染函数
   */
  action?: string | (() => VNode);
  /**
  * 头像区域的内容，可为渲染函数
  */
  avatar?: string | (() => VNode);
}
type CNoticeOptions = {
  type: 'info' | 'error' | 'success' | 'warning';
} & NoticeOptions;


type DialogOptions = {
  /**
   * 是否显示边框
   */
  bordered?: boolean;
  /**
   * 图标位置
   * @default 'left'
   */
  'icon-placement'?: 'left' | 'top';
  /**
   * 图标
   */
  icon?: (() => VNode);
  /**
   * 是否展示 loading 状态
   * @default false
   */
  loading?: boolean;
  /**
   * 点击遮罩直接关闭
   * @default true
   */
  'mask-closable'?: boolean;
  /**
   * 取消按钮文本，不填则无按钮
   */
  'negative-text'?: string;
  /**
   * 确认按钮文本，不填则无按钮
   */
  'positive-text'?: string;
  /**
   * 是否展示 icon
   * @default true
   */
  'show-icon'?: boolean;
  /**
   * 按钮行为，默认都是关闭
   */
  onClose?: (() => boolean) | Promise<boolean> | any;
  onNegativeClick?: (() => boolean) | Promise<boolean> | any;
  onPositiveClick?: (() => boolean) | Promise<boolean> | any;
  /**
   * 点击遮罩的回调
   */
  onMaskClick?: () => void;
} & NoticeOptions;

type CDialogOptions = {
  /**
   * @default 'warning'
   */
  type?: 'warning' | 'error' | 'success';
} & DialogOptions;

declare interface Window {
  $loadingBar: {
    start: () => void;
    error: () => void;
    finish: () => void;
  };

  $message: {
    destroyAll: () => void;
    create(content: string | (() => VNode), options?: CMessageOptions): any;
    success(content: string | (() => VNode), options?: MessageOptions): any;
    warning(content: string | (() => VNode), options?: MessageOptions): any;
    error(content: string | (() => VNode), options?: MessageOptions): any;
    info(content: string | (() => VNode), options?: MessageOptions): any;
    loading(content: string | (() => VNode), options?: MessageOptions): any;
  };

  $notice: {
    create(options: CNoticeOptions): any;
    info(options: NoticeOptions): any;
    success(options: NoticeOptions): any;
    warning(options: NoticeOptions): any;
    error(options: NoticeOptions): any;
    destroyAll: () => void;
  };

  $alert: {
    destroyAll: () => void;
    create(options: CDialogOptions): any;
    success(options: DialogOptions): any;
    warning(options: DialogOptions): any;
    error(options: DialogOptions): any;
    info(options: DialogOptions): any;
  }
}