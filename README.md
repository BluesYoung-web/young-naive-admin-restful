# young-naive-admin-restful

**RESTFUL API，与 [modern-server-egg](https://gitee.com/BluesYoung-web/modern-server-egg) 配套使用**

## 特性

Vue3 + TypeScript + Vite

Naive UI

Unocss

## 推荐开发环境

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).


## 使用

```sh
# 依赖安装
pnpm install
```

## 环境变量

```bash
# .env.local 文件

# 标题，选配
VITE_TITLE = 'XX后台'

# CSRF 默认启用，需要显示关闭
# VITE_DISABLE_CSRF = true

# casdoor 默认关闭，需要显示启用
# VITE_ENABLE_CASDOOR = true

# 代理接口的地址
VITE_BASE_HTTP = '/api'
# websocket 地址
VITE_BASE_WS = '/ws'
# 开发阶段的真实请求地址
VITE_DEV_ADDRESS = 'http://localhost:8080'

# casdoor 相关
VITE_CAS_SERVER = 'https://door.casdoor.com'
VITE_CAS_ID = '014ae4bd048734ca2dea'
VITE_CAS_ORG = 'casbin'
VITE_CAS_APP = 'app-casnode'
```

### 开发运行

```sh
pnpm dev
```

### 检查TS类型并打包

```sh
pnpm build
```

### 执行单元测试(开发阶段)

基于[Vitest](https://vitest.dev/)

```sh
pnpm test
```
